-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- H�te : 127.0.0.1
-- G�n�r� le :  mar. 16 jan. 2018 � 16:32
-- Version du serveur :  10.1.28-MariaDB
-- Version de PHP :  7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de donn�es :  `proxiv4sako`
--

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE `client` (
  `idClient` int(11) NOT NULL,
  `codePostal` int(11) NOT NULL,
  `numero` int(11) NOT NULL,
  `rue` varchar(255) DEFAULT NULL,
  `ville` varchar(255) DEFAULT NULL,
  `emailClient` varchar(255) DEFAULT NULL,
  `nomClient` varchar(255) DEFAULT NULL,
  `prenomClient` varchar(255) DEFAULT NULL,
  `telClient` varchar(255) DEFAULT NULL,
  `tyClient` varchar(255) DEFAULT NULL,
  `idEmploye` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- D�chargement des donn�es de la table `client`
--

INSERT INTO `client` (`idClient`, `codePostal`, `numero`, `rue`, `ville`, `emailClient`, `nomClient`, `prenomClient`, `telClient`, `tyClient`, `idEmploye`) VALUES
(1, 93310, 52, 'rue Gutenberg', 'Le Pre Saint Gervais', 'kogulan.rajendran@live.fr', 'Rajendran', 'Kogulan', '0651218201', 'PARTICULIER', 7),
(2, 28036, 1, 'Avenida de Concha Espina', 'Madrid', 'ronaldo.cristiano@gmail.fr', 'Ronaldo', 'Cristiano', '012345678', 'PARTICULIER', 7),
(3, 92750, 5, 'rue Gargand', 'Maner', 'Jean.Luc@hotmail.fr', 'Luc', 'Jean', '0651218201', 'ENTREPRISE', 7);

-- --------------------------------------------------------

--
-- Structure de la table `compte`
--

CREATE TABLE `compte` (
  `type_comtpe` varchar(31) NOT NULL,
  `idCompte` int(11) NOT NULL,
  `dateOuvertureCompte` varchar(255) DEFAULT NULL,
  `soldeCompte` float DEFAULT NULL,
  `decouvertAutoriser` float DEFAULT NULL,
  `tauxRemuneration` double DEFAULT NULL,
  `idClient` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `employe`
--

CREATE TABLE `employe` (
  `type_employe` varchar(31) NOT NULL,
  `idEmploye` int(11) NOT NULL,
  `codePostal` int(11) NOT NULL,
  `numero` int(11) NOT NULL,
  `rue` varchar(255) DEFAULT NULL,
  `ville` varchar(255) DEFAULT NULL,
  `login` varchar(255) DEFAULT NULL,
  `mdp` varchar(255) DEFAULT NULL,
  `nomEmploye` varchar(255) DEFAULT NULL,
  `prenomEmploye` varchar(255) DEFAULT NULL,
  `gerant_idEmploye` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- D�chargement des donn�es de la table `employe`
--

INSERT INTO `employe` (`type_employe`, `idEmploye`, `codePostal`, `numero`, `rue`, `ville`, `login`, `mdp`, `nomEmploye`, `prenomEmploye`, `gerant_idEmploye`) VALUES
('Conseiller', 1, 93310, 52, 'rue Gutenberg', 'Le Pr� Saint Gervais', 'Shuyin', '1234', 'Rajendran', 'Kogulan', NULL),
('Conseiller', 2, 85423, 25, 'Palo Alto', 'California', 'Mark', 'ilovefacebook', 'Zuckerberg', 'Mark', NULL),
('Conseiller', 3, 20005, 66, 'Peter Jennings Way', ' New York', 'Denn', 'password', 'Dennett', 'Daniel', NULL),
('Conseiller', 4, 90048, 6300, 'Wilshire Blvd', 'Los Angeles', 'sam', 'science', 'Harris', 'Sam', NULL),
('Gerant', 5, 1246, 64, 'Garden Rosemoor', 'Washington', 'Richard', 'goddelusion', 'Dawkins', 'Richard', NULL),
('Gerant', 6, 20005, 2022, 'Columbia Rd NW Apt 701', ' Washington', 'Hitch', 'godisnotgreat', 'Hitchens', 'Christopher', NULL),
('Conseiller', 7, 0, 0, NULL, NULL, 'admin', 'pass', 'Admin', 'main', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `transaction`
--

CREATE TABLE `transaction` (
  `idTransac` int(11) NOT NULL,
  `dateTrans` varchar(255) DEFAULT NULL,
  `montant` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour les tables d�charg�es
--

--
-- Index pour la table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`idClient`),
  ADD KEY `FK_118799c635a447e7a33414a9da9` (`idEmploye`);

--
-- Index pour la table `compte`
--
ALTER TABLE `compte`
  ADD PRIMARY KEY (`idCompte`),
  ADD KEY `FK_70dc4b4803534254b20f8a7a8b3` (`idClient`);

--
-- Index pour la table `employe`
--
ALTER TABLE `employe`
  ADD PRIMARY KEY (`idEmploye`),
  ADD UNIQUE KEY `UK_e850264494f9492b8ecb8d0543e` (`login`),
  ADD KEY `FK_292a7fa493dd4232a8525a6434f` (`gerant_idEmploye`);

--
-- Index pour la table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`idTransac`);

--
-- AUTO_INCREMENT pour les tables d�charg�es
--

--
-- AUTO_INCREMENT pour la table `client`
--
ALTER TABLE `client`
  MODIFY `idClient` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `compte`
--
ALTER TABLE `compte`
  MODIFY `idCompte` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `employe`
--
ALTER TABLE `employe`
  MODIFY `idEmploye` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `idTransac` int(11) NOT NULL AUTO_INCREMENT;

--
-- Contraintes pour les tables d�charg�es
--

--
-- Contraintes pour la table `client`
--
ALTER TABLE `client`
  ADD CONSTRAINT `FK_118799c635a447e7a33414a9da9` FOREIGN KEY (`idEmploye`) REFERENCES `employe` (`idEmploye`);

--
-- Contraintes pour la table `compte`
--
ALTER TABLE `compte`
  ADD CONSTRAINT `FK_70dc4b4803534254b20f8a7a8b3` FOREIGN KEY (`idClient`) REFERENCES `client` (`idClient`);

--
-- Contraintes pour la table `employe`
--
ALTER TABLE `employe`
  ADD CONSTRAINT `FK_292a7fa493dd4232a8525a6434f` FOREIGN KEY (`gerant_idEmploye`) REFERENCES `employe` (`idEmploye`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
