<%@include file="includes/header.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<h2>Formulaire d'inscription d'un client</h2>
<form:form method="POST" action="${pageContext.request.contextPath}/saveClient" modelAttribute="newClient" >
	<p>
		<i>Compl�tez le formulaire. Les champs marqu� par </i><em>*</em>
		<em>obligatoires</em>
	</p>
	<fieldset>
	<legend>Contact</legend>
		<label for="nom">Nom <em>*</em></label><form:input type="text" path="nomClient" /><br>
		<label for="prenom">Prenom<em>*</em></label><form:input type="text" path="prenomClient" /><br>
		<label for="adresse">Adresse <em>*</em></label>
	      <input type="text" id="numAdresse" name="numAdresse" value="" >
	      <input type="text" id="nomAdresse" name="nomAdresse" value="" ><br>
	    <label for="codePostal">Code Postal <em>*</em></label><input type="number" id="codePostal" name="codePostal" /><br>
	    <label for="ville">Ville <em>*</em></label> <input type="text" id="ville" name="ville"/><br>
	    <label for="telephone">Telephone</label><form:input type="tel" placeholder="06xxxxxxxx" path="telClient"/><br>
	    <label for="email">Email <em>*</em></label><form:input path="emailClient" type="email" placeholder="prenom.nom@gmail.com" /><br>
	</fieldset>
<fieldset>
		<legend>Information administrative</legend>
		<label for="typeClient">typeClient<em>*</em></label> <select id="typeClient" name="typeClient">
			<option value="Particulier">Particulier</option>
			<option value="Entreprise">Entreprise</option>
		</select><br>
<!-- 		<label for="idConseiller">Conseiller<em>*</em></label> -->
<!-- 		 <select name="conseiller"> -->
<%-- 			<c:forEach name = "Conseiller" var="item" items="${ListConseiller}"> --%>
<%-- 				<option value="${item}">${item.idEmploye}.&nbsp;${item.nomEmploye}&nbsp;${item.prenomEmploye}</option> --%>
<%-- 			</c:forEach> --%>
<!-- 		</select> -->
</fieldset>
		<p>
		<input name="submit" type="submit" id="submit" value="Validez" />
	</p>
</form:form>
<form action="${pageContext.request.contextPath}/listClient" method="get">
<p><input type="submit" value="Retour"></p>
<%@include file="includes/footer.jsp"%>