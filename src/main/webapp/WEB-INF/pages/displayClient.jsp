<%@include file="/WEB-INF/pages/includes/header.jsp"%>
<h1>Liste de vos clients</h1>
<fieldset>
	<!--     <legend>Vos clients</legend>   -->
	<table>
		<tr class="en-tete">
			<td>Id</td>
			<td>Nom</td>
			<td>Prenom</td>
			<td>Email</td>
			<td>Telephone</td>
			<td>Modification</td>
			<td>Virement</td>
<!-- 			<td>Ajout Compte</td> -->
			<td>Supprimer</td>
		</tr>

		<c:forEach var="asd" items="${tableauClient}">
			<tr class="ligne-paire">
				<td><c:out value="${asd.idClient}" /></td>
				<td><c:out value="${asd.nomClient}" /></td>
				<td><c:out value="${asd.prenomClient}" /></td>
				<td><c:out value="${asd.emailClient}" /></td>
				<td><c:out value="${asd.telClient}" /></td>
				<td><a
					href="${pageContext.request.contextPath}/updateClient?idClient=${asd.idClient}"
					class="btn btn-info" role="button">Modification</a></td>
				<td><a href="#"
					class="btn btn-info" role="button">Virement</a></td>
				<td><a href="${pageContext.request.contextPath}/deleteClient?idClient=${asd.idClient}" onclick="return confirm('�tes-vous s�r de vouloir supprimer ce client ?');"
					class="btn btn-info" role="button">Supprimer</a></td>
				<!--  	
	<td><a href="ServletVirement?idclient=${asd.idClient}"
		 class="btn btn-info" role="button">Virement</a></td>
	-->
			</tr>
		</c:forEach>

	</table>
</fieldset>
<%@include file="includes/footer.jsp"%>