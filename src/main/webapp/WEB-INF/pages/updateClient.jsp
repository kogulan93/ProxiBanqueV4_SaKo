<%@include file="/WEB-INF/pages/includes/header.jsp" %>
	
<h2>Mise a jour information Client</h2>
<form action="${pageContext.request.contextPath}/updateClientDAO" method="post">
  <p><i>Modifier le formulaire. Les champs marqu� par </i><em>*</em> sont <em>obligatoires</em></p>
    <fieldset>    
    <legend>Information Client</legend>  
    <label for="idClient">Id Client</label>  
    <input type="text" id="idClient" name="idClient" value="${InfoClient.idClient}" readonly><br>
  </fieldset> 
  <fieldset>
    <legend>Contact</legend>
      <label for="nom">Nom <em>*</em></label>
      <input type="text" id="nom" name="nom" value="${InfoClient.nomClient}" ><br>
      
      <label for="prenom">Prenom <em>*</em></label>
      <input type="text" id="prenom" name="prenom" value="${InfoClient.prenomClient}" required><br>
      
      <label for="adresse">Adresse <em>*</em></label>
      <input type="text" id="numAdresse" name="numAdresse" value="${InfoAdresse.numero}" required>
      <input type="text" id="nomAdresse" name="nomAdresse" value="${InfoAdresse.rue}" required><br>
      
      <label for="codePostal">Code Postal <em>*</em></label>
      <input type="text" id="codePostal" name="codePostal" value="${InfoAdresse.codePostal}" required><br>
      
      <label for="ville">Ville <em>*</em></label>
      <input type="text" id="ville" name="ville" value="${InfoAdresse.ville}" required><br>
      
      <label for="telephone">Telephone</label>           
      <input id="telephone" type="tel" placeholder="06xxxxxxxx" name="telephone" value="${InfoClient.telClient}"><br>
      
      <label for="email">Email <em>*</em></label>
      <input id="email" name="email" type="email" placeholder="prenom.nom@gmail.com" value="${InfoClient.emailClient}" required><br>
  </fieldset>
  <fieldset>
    <legend>Information administrative</legend>    
      <label for="typeClientL">typeClient<em>*</em></label>
      <select id="typeClient" name="typeClient">
      <c:choose>
    		<c:when test="${InfoClient.tyClient=='ENTREPRISE'}">
        		 <option value="Particulier">Particulier</option>
        		 <option value="Entreprise" selected>Entreprise</option>
       		<br />
    		</c:when>    
    		<c:otherwise>
        		 <option value="Particulier" selected>Particulier</option>
        		 <option value="Entreprise">Entreprise</option>
        	<br />
    		</c:otherwise>
	</c:choose>
      </select><br>
      
      <label for="idConseiller">Id Conseiller</label>
		<input type="text" id="idConseiller" name="idConseiller" value="${InfoConseiller.idEmploye}"><br>
  </fieldset> 
<c:if test="${DisplayCC}">				
<h2>Information relative aux comptes bancaires </h2>  


 <fieldset> 
 <legend>Compte Courant</legend>   
 	  <input type="hidden" id="idCompteCC" name="idCompteCC" value="${IDCompteCC}"><br> 
      <label for="soldeCC">Solde Compte</label>
      <input type="text" name="soldeCC" id="soldeCC" value="${SoldeCompteC}" ><br>
      <label for="dateOuvertureCC">Date d'ouverture</label>
      <input type="date" id="dateOuvertureCC" name="dateOuvertureCC" placeholder="DD/MM/YYY" value="${DateOuvertureCompteC}" ><br> 
      <label for="decouvertA">D�couvert autoris�</label>
      <input type="text" id="decouvertA" name="decouvertA" value="${DecouvertA}" ><br>      
  </fieldset> 
   </c:if>
  <c:if test="${DisplayCE}"> 
  <fieldset>    
    <legend>Compte Epargne</legend>    
      <input type="hidden" id="idCompteCE" name="idCompteCE" value="${IDCompteCE}"><br>
      <label for="soldeCE">Solde Compte</label>
      <input type="text" id="soldeCE" name="soldeCE" value="${SoldeCompteE}" ><br>
      
      <label for="dateOuvertureCE">Date d'ouverture</label>
      <input type="date" id="dateOuvertureCE" name="dateOuvertureCE" placeholder="DD/MM/YYY" value="${DateOuvertureCompteE}" ><br>
      
      <label for="tauxRemun">Taux de r�mun�ration</label>
      <input type="text" id="tauxRemun" name="tauxRemun" value="${TauxRemuneration}" ><br>       
  </fieldset> 
  </c:if>
  <p><input type="submit" value="Sauvegarder"></p>
</form>
<form action="${pageContext.request.contextPath}/listClient" method="get">
<p><input type="submit" value="Retour"></p>
<%@include file="includes/footer.jsp" %>