<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<!DOCTYPE html>
<html lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet" />
<link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet"/>
<link href="${pageContext.request.contextPath}/resources/css/styleheader.css" rel="stylesheet"/>

<script src="${pageContext.request.contextPath}/resources/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>

<title>Login</title>
</head>
<body>
	<p>ProxiBanque</p>
	
	<form action="${pageContext.request.contextPath}/loginEmploye" method="post" >
		<h1>
			<img src="${pageContext.request.contextPath}/resources/css/images/logo.png"
				alt="Logo de ProxiBanque"
				height="200px" width="300px"/>
		</h1>
		<fieldset id="inputs">
			<input id="login" type="text" name = "login" placeholder="nom de l'utilisateur" autofocus
				required> <input id="password" type="password" name = "mdp"
				placeholder="mot de passe" required>
		</fieldset>
		<fieldset id="actions">
			<input type="submit" id="submit" value="se connecter">

		</fieldset>
	</form>
</body>
</html>