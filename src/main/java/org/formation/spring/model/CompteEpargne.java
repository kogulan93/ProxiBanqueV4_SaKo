package org.formation.spring.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * @author Kogulan - Sarra La classe CompteEpargne h�rite de Compte Le taux de
 *         remun�ration est fixer par defaut � 0.03%
 */
@Entity
@DiscriminatorValue("EPARGNE")
public class CompteEpargne extends Compte {
	private double tauxRemuneration = 0.03;

	// Constructeurs
	public CompteEpargne() {
		super();
	}

	/**
	 * Constructeur permettant de fixer le solde du compte et une date d'ouverture
	 * different de la date du jour
	 * 
	 * @param soldeCompte
	 * @param dateOuvertureCompte
	 */
	public CompteEpargne(float soldeCompte, String dateOuvertureCompte) {
		super(soldeCompte, dateOuvertureCompte);
	}

	/**
	 * Constructeur permettant de fixer le solde du compte et un taux de
	 * remuneration
	 * 
	 * @param soldeCompte
	 * @param tauxRemuneration
	 */
	public CompteEpargne(float soldeCompte, double tauxRemuneration) {
		super(soldeCompte);
		this.tauxRemuneration = tauxRemuneration;
	}

	/**
	 * Constructeur permettant de fixer le solde du compte, une date d'ouverture et
	 * un taux de remuneration
	 * 
	 * @param soldeCompte
	 * @param dateOuvertureCompte
	 * @param tauxRemuneration
	 */
	public CompteEpargne(float soldeCompte, String dateOuvertureCompte, double tauxRemuneration) {
		super(soldeCompte, dateOuvertureCompte);
		this.tauxRemuneration = tauxRemuneration;
	}

	// Getters
	public double getTauxRemuneration() {
		return tauxRemuneration;
	}

	// Setters
	public void setTauxRemuneration(double tauxRemuneration) {
		this.tauxRemuneration = tauxRemuneration;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.formation.proxibanque.model.Compte#toString()
	 */
	public String toString() {

		return super.toString() + "\nLe taux de r�muneration est de " + this.tauxRemuneration;
	}

}
