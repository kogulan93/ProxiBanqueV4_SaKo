package org.formation.spring.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 * @author Kogulan - Sarra Classe m�re permettant de definir les comptes
 *         bancaires (courant et epargne)
 *
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type_comtpe")
@DiscriminatorValue("mainCompte")
public abstract class Compte {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	protected int idCompte;

	@Column(precision = 10, scale = 2)
	protected float soldeCompte;
	protected String dateOuvertureCompte;

	@OneToOne
	@JoinColumn(name = "idClient")
	private Client client;

	// Constructeur
	public Compte() {
		super();
	}

	/**
	 * Constructeur permettant de fixer le solde du compte, la date d'ouverture sera
	 * la date du jour
	 * 
	 * @param solde
	 */
	public Compte(float solde) {
		super();
		this.soldeCompte = solde;
		this.dateOuvertureCompte = LocalDateTime.now().toString();
	}

	/**
	 * Constructeur permettant de fixer le solde du compte et la dateOuverture
	 * different de la date du jour
	 * 
	 * @param solde
	 * @param dateOuverture
	 */
	public Compte(float solde, String dateOuverture) {
		super();
		this.soldeCompte = solde;
		this.dateOuvertureCompte = dateOuverture;
	}

	// Getters
	public int getIdCompte() {
		return idCompte;
	}

	public float getSoldeCompte() {
		return soldeCompte;
	}

	public String getDateOuvertureCompte() {
		return dateOuvertureCompte;
	}

	public Client getClient() {
		return client;
	}

	// Setters
	public void setIdCompte(int idCompte) {
		this.idCompte = idCompte;
	}

	public void setDateOuvertureCompte(String dateOuvertureCompte) {
		this.dateOuvertureCompte = dateOuvertureCompte;
	}

	public void setSoldeCompte(float soldeCompte) {
		this.soldeCompte = soldeCompte;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	@Override
	public String toString() {
		return "Compte [idCompte=" + idCompte + ", soldeCompte=" + soldeCompte + ", dateOuvertureCompte="
				+ dateOuvertureCompte + "]";
	}

}
