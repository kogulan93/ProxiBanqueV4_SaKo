package org.formation.spring.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author Kogulan - sarra
 *
 */
@Entity
@Component
@Scope("prototype")
public class Transaction {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idTransac;
	private String dateTrans;
	private double montant;

	// CONSTRUCTEURS
	/**
	 * 
	 */
	public Transaction() {
		super();
	}

	/**
	 * @param idTransac
	 * @param montant
	 */
	public Transaction(int idTransac, double montant) {
		super();
		this.idTransac = idTransac;
		this.montant = montant;
	}

	/**
	 * @param idTransac
	 * @param dateTrans
	 * @param montant
	 */
	public Transaction(int idTransac, String dateTrans, double montant) {
		super();
		this.idTransac = idTransac;
		this.dateTrans = dateTrans;
		this.montant = montant;
	}

	// GETTERS & SETTERS
	public int getIdTransac() {
		return idTransac;
	}

	public void setIdTransac(int idTransac) {
		this.idTransac = idTransac;
	}

	public String getDateTrans() {
		return dateTrans;
	}

	public void setDateTrans(String dateTrans) {
		this.dateTrans = dateTrans;
	}

	public double getMontant() {
		return montant;
	}

	public void setMontant(double montant) {
		this.montant = montant;
	}

}
