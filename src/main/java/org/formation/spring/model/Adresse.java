package org.formation.spring.model;

import javax.persistence.Embeddable;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author Kogulan - Sarra La classe Adresse est une classe d'objets inclus dans
 *         Client et Employe
 *
 */
@Component
@Scope("prototype")
@Embeddable
public class Adresse {
	private int numero;
	private String rue;
	private int codePostal;
	private String ville;

	// CONSTRUCTEURS

	/**
	 * Constructeur par defaut
	 */
	public Adresse() {

	}

	/**
	 * Constructeur prenant en paramatre un num�ro de voie, le nom de la voie, le
	 * codePostal et le nom de la Ville
	 * 
	 * @param numero
	 * @param rue
	 * @param codePostal
	 * @param ville
	 */
	public Adresse(int numero, String rue, int codePostal, String ville) {
		this.numero = numero;
		this.rue = rue;
		this.codePostal = codePostal;
		this.ville = ville;
	}

	// GETTERS & SETTERS
	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getRue() {
		return rue;
	}

	public void setRue(String rue) {
		this.rue = rue;
	}

	public int getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(int codePostal) {
		this.codePostal = codePostal;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	@Override
	public String toString() {
		return "Adresse [numero=" + numero + ", rue=" + rue + ", codePostal=" + codePostal + ", ville=" + ville + "]";
	}
}
