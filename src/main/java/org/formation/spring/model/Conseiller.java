package org.formation.spring.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * @author Kogulan - Sarra La classe Conseiller h�rite de Employe
 *
 */
@Entity
public class Conseiller extends Employe {
	// ATTRIBUTS
	@ManyToOne
	private Gerant gerant;

	@OneToMany(mappedBy = "conseiller")
	private List<Client> clients;

	// CONSTRUCTEURS
	/**
	 * Constructeur par d�faut
	 */
	public Conseiller() {
		super();
		clients = new ArrayList<Client>();
	}

	/**
	 * @param nomEmploye
	 * @param prenomEmploye
	 * @param adresse
	 * @param login
	 * @param mdp
	 */
	public Conseiller(String nomEmploye, String prenomEmploye, Adresse adresse, String login, String mdp) {
		super(nomEmploye, prenomEmploye, adresse, login, mdp);
		clients = new ArrayList<Client>();
	}

	// GETTERS ET SETTERS
	public List<Client> getListeClients() {
		return clients;
	}

	public void setListeClients(List<Client> listeClients) {
		this.clients = listeClients;
	}

	public void addClient(Client c) {
		clients.add(c);
	}

	public List<Client> getClients() {
		return clients;
	}

	public void setClients(List<Client> clients) {
		this.clients = clients;
	}

	public Gerant getGerant() {
		return gerant;
	}

	public void setGerant(Gerant gerant) {
		this.gerant = gerant;
	}

}
