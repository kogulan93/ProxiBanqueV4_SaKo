package org.formation.spring.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Kogulan - Sarra Classe m�re permettant de definir les types
 *         d'employes (G�rant/Conseiller)
 *
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type_employe")
public abstract class Employe {
	// ATTRIBUTS
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idEmploye;

	protected String nomEmploye;
	protected String prenomEmploye;

	@Autowired
	@Embedded
	protected Adresse adresse;

	@Column(unique = true)
	protected String login;
	protected String mdp;

	// CONSTRUCTEURS

	/**
	 * Constructeur par d�faut
	 */
	public Employe() {
		super();
	}

	/**
	 * @param nomEmploye
	 * @param prenomEmploye
	 * @param adresse
	 * @param login
	 * @param mdp
	 */
	public Employe(String nomEmploye, String prenomEmploye, Adresse adresse, String login, String mdp) {
		super();
		this.nomEmploye = nomEmploye;
		this.prenomEmploye = prenomEmploye;
		this.adresse = adresse;
		this.login = login;
		this.mdp = mdp;
	}

	public int getIdEmploye() {
		return idEmploye;
	}

	public void setIdEmploye(int idEmploye) {
		this.idEmploye = idEmploye;
	}

	public String getNomEmploye() {
		return nomEmploye;
	}

	public void setNomEmploye(String nomEmploye) {
		this.nomEmploye = nomEmploye;
	}

	public String getPrenomEmploye() {
		return prenomEmploye;
	}

	public void setPrenomEmploye(String prenomEmploye) {
		this.prenomEmploye = prenomEmploye;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getMdp() {
		return mdp;
	}

	public void setMdp(String mdp) {
		this.mdp = mdp;
	}

	@Override
	public String toString() {
		return "Employe [idEmploye=" + idEmploye + ", nomEmploye=" + nomEmploye + ", prenomEmploye=" + prenomEmploye
				+ ", adresse=" + adresse + ", login=" + login + ", mdp=" + mdp + "]";
	}

}
