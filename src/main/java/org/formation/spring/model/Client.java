package org.formation.spring.model;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.springframework.beans.factory.annotation.Autowired;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Client {
	// Attributs
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idClient;
	private String nomClient;
	private String prenomClient;

	@Autowired
	@Embedded
	private Adresse adresse;

	private String telClient;
	private String emailClient;

	@Enumerated(EnumType.STRING)
	private TypeClient tyClient;

	@OneToOne(cascade = CascadeType.ALL, mappedBy = "client")
	private CompteCourant compteCourant;

	@OneToOne(cascade = CascadeType.ALL, mappedBy = "client")
	private CompteEpargne compteEpargne;

	@ManyToOne
	@JoinColumn(name = "idEmploye", referencedColumnName = "idEmploye")
	private Conseiller conseiller;

	// Constructeur
	/**
	 * Constructeur par d�faut
	 */
	public Client() {
		super();
	}

	/**
	 * Constructeur de Client
	 * 
	 * @param nomClient
	 * @param prenomClient
	 * @param adresse
	 * @param telClient
	 * @param emailClient
	 * @param tyClient
	 */
	public Client(String nomClient, String prenomClient, Adresse adresse, String telClient, String emailClient,
			TypeClient tyClient) {
		super();
		this.nomClient = nomClient;
		this.prenomClient = prenomClient;
		this.adresse = adresse;
		this.telClient = telClient;
		this.emailClient = emailClient;
		this.tyClient = tyClient;
	}

	/**
	 * Constructeur de Client prenant en param�tre un Conseiller
	 * 
	 * @param nomClient
	 * @param prenomClient
	 * @param adresse
	 * @param telClient
	 * @param emailClient
	 * @param tyClient
	 * @param conseiller
	 */
	public Client(String nomClient, String prenomClient, Adresse adresse, String telClient, String emailClient,
			TypeClient tyClient, Conseiller conseiller) {
		super();
		this.nomClient = nomClient;
		this.prenomClient = prenomClient;
		this.adresse = adresse;
		this.telClient = telClient;
		this.emailClient = emailClient;
		this.tyClient = tyClient;
		this.conseiller = conseiller;
	}

	/**
	 * Constructeur de Client prenant en param�tre un Conseiller, un compte courant
	 * et un compte epargne
	 * 
	 * @param nomClient
	 * @param prenomClient
	 * @param adresse
	 * @param telClient
	 * @param emailClient
	 * @param tyClient
	 * @param compteCourant
	 * @param compteEpargne
	 * @param conseiller
	 */
	public Client(String nomClient, String prenomClient, Adresse adresse, String telClient, String emailClient,
			TypeClient tyClient, CompteCourant compteCourant, CompteEpargne compteEpargne, Conseiller conseiller) {
		super();
		this.nomClient = nomClient;
		this.prenomClient = prenomClient;
		this.adresse = adresse;
		this.telClient = telClient;
		this.emailClient = emailClient;
		this.tyClient = tyClient;
		this.compteCourant = compteCourant;
		this.compteEpargne = compteEpargne;
		this.conseiller = conseiller;
	}

	// GETTERS & SETTERS
	public int getIdClient() {
		return idClient;
	}

	public void setConseiller(Employe conseiller) {
		this.conseiller = (Conseiller) conseiller;
	}

	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}

	public String getNomClient() {
		return nomClient;
	}

	public void setNomClient(String nomClient) {
		this.nomClient = nomClient;
	}

	public String getPrenomClient() {
		return prenomClient;
	}

	public void setPrenomClient(String prenomClient) {
		this.prenomClient = prenomClient;
	}

	public String getTelClient() {
		return telClient;
	}

	public void setTelClient(String telClient) {
		this.telClient = telClient;
	}

	public String getEmailClient() {
		return emailClient;
	}

	public void setEmailClient(String emailClient) {
		this.emailClient = emailClient;
	}

	public TypeClient getTyClient() {
		return tyClient;
	}

	public void setTyClient(TypeClient tyClient) {
		this.tyClient = tyClient;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	public CompteCourant getCompteCourant() {
		return compteCourant;
	}

	public void setCompteCourant(CompteCourant compteCourant) {
		this.compteCourant = compteCourant;
	}

	public CompteEpargne getCompteEpargne() {
		return compteEpargne;
	}

	public void setCompteEpargne(CompteEpargne compteEpargne) {
		this.compteEpargne = compteEpargne;
	}

	public Employe getConseiller() {
		return conseiller;
	}

	public void setConseiller(Conseiller conseiller) {
		this.conseiller = conseiller;
	}

	@Override
	public String toString() {
		return "Client [idClient=" + idClient + ", nomClient=" + nomClient + ", prenomClient=" + prenomClient
				+ ", adresse=" + adresse + ", telClient=" + telClient + ", emailClient=" + emailClient + ", tyClient="
				+ tyClient + "]";
	}

}
