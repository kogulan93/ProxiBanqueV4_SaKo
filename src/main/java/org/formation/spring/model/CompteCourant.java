package org.formation.spring.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * @author Kogulan - Sarra La classe CompteCourant h�rite de Compte Le decouvert
 *         autoriser est fixer par defaut � 1000
 */
@Entity
@DiscriminatorValue("COURANT")
public class CompteCourant extends Compte {
	private float decouvertAutoriser = 1000;

	// Constructeurs
	public CompteCourant() {
		super();
	}

	/**
	 * @param soldeCompte
	 */
	public CompteCourant(float soldeCompte) {
		super(soldeCompte);
	}

	/**
	 * Constructeur permettant de fixer le solde du compte et le montant du
	 * d�couvert autoriser
	 * 
	 * @param soldeCompte
	 * @param decouvertAutoriser
	 */
	public CompteCourant(float soldeCompte, float decouvertAutoriser) {
		super(soldeCompte);
		this.decouvertAutoriser = decouvertAutoriser;
	}

	/**
	 * Constructeur permettant de fixer le solde du compte et une date d'ouverture
	 * 
	 * @param soldeCompte
	 * @param dateOuvertureCompte
	 */
	public CompteCourant(float soldeCompte, String dateOuvertureCompte) {
		super(soldeCompte, dateOuvertureCompte);
	}

	/**
	 * Constructeur prenant en param�tre le solde du compte, une date d'ouverture et
	 * le montant du d�couvert autoriser
	 * 
	 * @param soldeCompte
	 * @param dateOuvertureCompte
	 * @param decouvert
	 */
	public CompteCourant(float soldeCompte, String dateOuvertureCompte, float decouvert) {
		super(soldeCompte, dateOuvertureCompte);
		this.decouvertAutoriser = decouvert;
	}

	// Getters
	public float getDecouvertA() {
		return decouvertAutoriser;
	}

	public float getDecouvertAutoriser() {
		return decouvertAutoriser;
	}

	// Setters
	public void setDecouvertA(float decouvertA) {
		this.decouvertAutoriser = decouvertA;
	}

	public void setDecouvertAutoriser(float decouvertAutoriser) {
		this.decouvertAutoriser = decouvertAutoriser;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.formation.proxibanque.model.Compte#toString()
	 */
	public String toString() {
		return super.toString() + "\nLe d�couvert autoris� est de " + this.decouvertAutoriser;
	}
}
