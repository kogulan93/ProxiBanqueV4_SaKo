package org.formation.spring.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

@Entity
// @DiscriminatorValue("GERANT")
public class Gerant extends Employe {
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "gerant")
	private List<Conseiller> conseillers;

	public Gerant() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Gerant(String nomEmploye, String prenomEmploye, Adresse adresse, String login, String mdp) {
		super(nomEmploye, prenomEmploye, adresse, login, mdp);
		// TODO Auto-generated constructor stub
	}

}
