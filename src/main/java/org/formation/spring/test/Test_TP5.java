package org.formation.spring.test;

import java.util.List;

import org.formation.spring.config.ApplicationConfig;
import org.formation.spring.model.Adresse;
import org.formation.spring.model.Client;
import org.formation.spring.model.CompteCourant;
import org.formation.spring.model.Conseiller;
import org.formation.spring.model.Employe;
import org.formation.spring.model.TypeClient;
import org.formation.spring.service.IServiceClient;
import org.formation.spring.service.IServiceEmploye;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.test.context.web.WebAppConfiguration;

public class Test_TP5 {

	public static void main(String[] args) {
	ApplicationContext  context = new AnnotationConfigApplicationContext(ApplicationConfig.class);		   


		IServiceClient servCli = context.getBean("serviceClient", IServiceClient.class);
		IServiceEmploye servEmp = context.getBean("serviceEmploye", IServiceEmploye.class);
		
		//servCli.listClients();
		//servCli.addClient(new Client("Rajendran","Kogulan","52 rue Gutenberg",93310,"Le Pre Saint Gervais","0651218201","kogulan.rajendran@live.fr",TypeClient.PARTICULIER));
		
		
//		servCli.addClient(new Client2("BritneyJPA","Spears","log2JPA","mdp2",new Adresse(36, "avenue du JPAmaine", "Paris")));
//		servCli.addClient(new Client2("Bernard","Tapie","log2","mdp2",new Adresse(36, "rue de la paix", "Paris")));

		
//		Client testCli = new Client("AZERTY","Jean",new Adresse(5,"rue Gargand",92750,"Maner"),"0651218201","Jean.Luc@hotmail.fr",TypeClient.ENTREPRISE);
//		CompteCourant cc = new CompteCourant(125000,"12/07/2000");
//		
//
//		cc.setClient(testCli);
//		testCli.setCompteCourant(cc);
//
//		
//		servCli.addClient(testCli);
//		
		Client c1 = servCli.selectById(1);
		
		Conseiller emp = (Conseiller) servEmp.findEmployeByidEmploye(1);
		System.out.println(emp.getAdresse());
//		emp.addClient(c1);
		
		servCli.addClient(c1);
//		for (Employe employe : a) {
//			System.out.println(employe.toString());
//		}
		//System.out.println(a.toString());
		
	    //System.out.println("!!!!!!!!!!\n"+servCli.listClients());

	    ((ConfigurableApplicationContext)(context)).close();

	}

}
