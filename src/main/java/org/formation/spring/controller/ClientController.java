package org.formation.spring.controller;

import java.util.List;

import org.formation.spring.model.Adresse;
import org.formation.spring.model.Client;
import org.formation.spring.model.Conseiller;
import org.formation.spring.model.Employe;
import org.formation.spring.model.TypeClient;
import org.formation.spring.service.ServiceClient;
import org.formation.spring.service.ServiceEmploye;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

/**
 * @author Kogulan - Sarra
 * Classe Controller Client
 * Regroupe les m�thodes permettant de gerer le crud
 *
 */
@Controller
@SessionAttributes("client")
public class ClientController {

	@Autowired
	private ServiceClient servCli;

	@Autowired
	private ServiceEmploye servEmpl;
	
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	//private static StringBuilder chaine = new StringBuilder();

	@ModelAttribute("client")
    public Client getClient () {
        return new Client();
    }
	
	@RequestMapping(value = "/updateClient{idClient}", method = RequestMethod.GET)
	public String updateClient(@RequestParam("idClient") String idClient, ModelMap model) {

		Client myClient = servCli.selectById(Integer.parseInt(idClient));
		model.addAttribute("InfoClient", myClient);
		model.addAttribute("InfoAdresse", myClient.getAdresse());
		model.addAttribute("InfoConseiller", myClient.getConseiller());
		LOGGER.info("Redirection vers la page: updateClient.jsp");
		return "updateClient";
	}


	@RequestMapping(value = "/addClient", method = RequestMethod.GET)
	public String displayAddClient(ModelMap model) {
		List<Employe> emp = servEmpl.listEmployes();
		
		Client myClient = new Client();
		Adresse myAddress = new Adresse();
		model.addAttribute("ListConseiller", emp);
		model.addAttribute("newClient",myClient);
		model.addAttribute("newAddress",myAddress);
		
		LOGGER.info("Redirection vers la page: addClient.jsp");
		return "addClient";
	}

	@RequestMapping(value = "/saveClient", method = RequestMethod.POST)
	public String saveClient(@ModelAttribute("newClient") Client client,
			@RequestParam("numAdresse") String numAdresse,
			@RequestParam("nomAdresse") String nomAdresse,
			@RequestParam("codePostal") String codePostal,
			@RequestParam("ville") String ville,
			@RequestParam("typeClient") String typeClient,	
			ModelMap model) {	
		servCli.addClient(new Client(client.getNomClient(),client.getPrenomClient(),new Adresse(Integer.parseInt(numAdresse),nomAdresse,Integer.parseInt(codePostal),ville),client.getTelClient(),client.getEmailClient(),TypeClient.ENTREPRISE, (Conseiller) servEmpl.findEmployeByidEmploye(7)));
		model.addAttribute("InfoTraitement","Le client a �t� correctement inserer dans la base de donn�es");
		LOGGER.info("L'op�ration: [insert Client] a �t� correctement effectu�e");	
		LOGGER.info("Redirection vers la page: pageValidation.jsp");
		return "pageValidation";
	}
	
	@RequestMapping(value = "/deleteClient{idClient}", method = RequestMethod.GET)
	public String deleteClient(@RequestParam("idClient") String idClient, ModelMap model) {	
		servCli.deleteClient(servCli.selectById(Integer.parseInt(idClient)));
		model.addAttribute("InfoTraitement","Le client a �t� correctement supprim� de la base de donn�e");
		LOGGER.info("L'op�ration: [delete Client] a �t� correctement effectu�e");
		LOGGER.info("Redirection vers la page: pageValidation.jsp");
		return "pageValidation";
	}

	@RequestMapping(value = "/updateClientDAO", method = RequestMethod.POST)
	public String updateClientDAO(ModelMap model,
			@RequestParam("idClient") String idClient,
			@RequestParam("nom") String nom,
			@RequestParam("prenom") String prenom,
			@RequestParam("numAdresse") String numAdresse,
			@RequestParam("nomAdresse") String nomAdresse,
			@RequestParam("codePostal") String codePostal,
			@RequestParam("ville") String ville,	
			@RequestParam("telephone") String tel,
			@RequestParam("email") String email,
			@RequestParam("typeClient") String typeClient 			
			) {	
		Client clientUpdate = servCli.selectById(Integer.parseInt(idClient));
		clientUpdate.setNomClient(nom);
		clientUpdate.setPrenomClient(prenom);
		clientUpdate.setAdresse(new Adresse(Integer.parseInt(numAdresse),nomAdresse,Integer.parseInt(codePostal),ville));
		clientUpdate.setTelClient(tel);
		clientUpdate.setEmailClient(email);
		clientUpdate.setTyClient(TypeClient.ENTREPRISE);		
		servCli.addClient(clientUpdate);
		model.addAttribute("InfoTraitement","Les donn�es ont �t� mises � jour");
		LOGGER.info("L'op�ration: [update Client] a �t� correctement effectu�e");
		LOGGER.info("Redirection vers la page: pageValidation.jsp");
		return "pageValidation";
	}

}
