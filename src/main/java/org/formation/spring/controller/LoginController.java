package org.formation.spring.controller;

import java.util.List;

import org.formation.spring.dao.CrudClientDAO;
import org.formation.spring.dao.CrudEmployeDAO;
import org.formation.spring.model.Client;
import org.formation.spring.model.Conseiller;
import org.formation.spring.model.Employe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;


/**
 * @author Kogulan - Sarra
 * Classe Controller Login permet de gerer les connections et les deplacements onglets
 * 
 *
 */
@Controller
@SessionAttributes("employe")
public class LoginController {

	@Autowired
	private CrudClientDAO crudClientDAO;

	@Autowired
	private CrudEmployeDAO crudEmployeDAO;
	
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	private static StringBuilder chaine = new StringBuilder();

	@ModelAttribute("employe")
    public Employe getConseiller () {
        return new Conseiller();
    }
	
	@RequestMapping(value = "/loginEmploye", method = RequestMethod.POST)
	public String recoverPass(@RequestParam("login") String login, @RequestParam("mdp") String mdp, ModelMap model) {
		Employe employe = crudEmployeDAO.findByLoginAndMdp(login, mdp);
		if (employe != null) {
			if (Conseiller.class.isAssignableFrom(employe.getClass())) {
				LOGGER.debug("Type Conseiller: " + employe.getNomEmploye()+" "+employe.getPrenomEmploye() + " ");				
				model.addAttribute("employe",employe);
				model.addAttribute("tableauClient",crudClientDAO.findAllClientByConseiller(employe));
				return "displayClient";
			} else {
				LOGGER.debug("Type G�rant: " + employe.getNomEmploye()+" "+employe.getPrenomEmploye() + " ");
				return "login";
			}
		} else {
			 model.addAttribute("TestMessage",true);
			 model.addAttribute("TextMessage","Incorrect username or password.");
			 return "login";
		}
	}
	@RequestMapping(value = "/deLog", method = RequestMethod.GET)
	public String logOut() {
		return "login";
	}
	
	@RequestMapping(value = "/listClient", method = RequestMethod.GET)
	public String displayClients(@ModelAttribute("employe") Conseiller conseiller, ModelMap model) {
		System.out.println(conseiller.getPrenomEmploye());
		model.addAttribute("conseiller",conseiller);
		model.addAttribute("tableauClient",crudClientDAO.findAllClientByConseiller(conseiller));
		return "displayClient";
	}
}
