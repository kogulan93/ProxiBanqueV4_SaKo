package org.formation.spring.service;

import java.util.List;

import javax.annotation.PostConstruct;

import org.formation.spring.dao.CrudEmployeDAO;
import org.formation.spring.model.Adresse;
import org.formation.spring.model.Conseiller;
import org.formation.spring.model.Employe;
import org.formation.spring.model.Gerant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Kogulan  - Sarra
 * 
 *
 */
@Service("serviceEmploye")
public class ServiceEmploye implements IServiceEmploye {
	private static final Logger LOGGER = LoggerFactory.getLogger(ServiceEmploye.class);

	@Autowired
	private CrudEmployeDAO crudEmployeDAO;

	/**
	 * Creation des Employes dans la base de donnees
	 */
	@PostConstruct
	public void createSomeEmploye() {
		// Conseiller
		addEmploye(new Conseiller("Rajendran", "Kogulan",
				new Adresse(52, "rue Gutenberg", 93310, "Le Pr� Saint Gervais"), "Shuyin", "1234"));
		addEmploye(new Conseiller("Zuckerberg", "Mark", new Adresse(25, "Palo Alto", 85423, "California"), "Mark",
				"ilovefacebook"));
		addEmploye(new Conseiller("Dennett", "Daniel", new Adresse(66, "Peter Jennings Way", 20005, " New York"),
				"Denn", "password"));
		addEmploye(new Conseiller("Harris", "Sam", new Adresse(6300, "Wilshire Blvd", 90048, "Los Angeles"), "sam",
				"science"));
		// Gerant
		addEmploye(new Gerant("Dawkins", "Richard", new Adresse(64, "Garden Rosemoor", 1246, "Washington"), "Richard",
				"goddelusion"));
		Gerant g1= new Gerant("Hitchens", "Christopher",
				new Adresse(2022, "Columbia Rd NW Apt 701", 20005, " Washington"), "Hitch", "godisnotgreat");
		addEmploye(g1);
		
		
		for (Employe emp : crudEmployeDAO.findAll()) {
			if(Conseiller.class.isAssignableFrom(emp.getClass())){
				((Conseiller)emp).setGerant(g1);
			}
		}

	}

	/* (non-Javadoc)
	 * @see org.formation.spring.service.IServiceEmploye#addEmploye(org.formation.spring.model.Employe)
	 */
	@Override
	public void addEmploye(Employe e) {
		LOGGER.info("Appel de la methode .save(Employe)");
		crudEmployeDAO.save(e);
	}

	/* (non-Javadoc)
	 * @see org.formation.spring.service.IServiceEmploye#listEmployes()
	 */
	@Override
	public List<Employe> listEmployes() {
		LOGGER.info("Appel de la methode findAll()");
		return crudEmployeDAO.findAll();
	}

	/* (non-Javadoc)
	 * @see org.formation.spring.service.IServiceEmploye#deleteEmploye(int)
	 */
	@Override
	public void deleteEmploye(int idEmploye) {
		LOGGER.info("Appel de la methode delete(idEmploye)");
		crudEmployeDAO.delete(idEmploye);
	}

	/* (non-Javadoc)
	 * @see org.formation.spring.service.IServiceEmploye#editEmploye(int)
	 */
	@Override
	public Employe editEmploye(int idEmploye) {
		LOGGER.info("Appel de la methode findOne(idEmploye)");
		return crudEmployeDAO.findOne(idEmploye);
	}

	/* (non-Javadoc)
	 * @see org.formation.spring.service.IServiceEmploye#updateEmploye(org.formation.spring.model.Employe)
	 */
	@Override
	public void updateEmploye(Employe c) {
		LOGGER.info("Appel de la methode .save(Employe)");
		crudEmployeDAO.save(c);
	}

	/* (non-Javadoc)
	 * @see org.formation.spring.service.IServiceEmploye#findEmployeByidEmploye(int)
	 */
	@Override
	public Employe findEmployeByidEmploye(int id) {
		LOGGER.info("Appel de la methode .findEmployeByidEmploye(id)");
		return crudEmployeDAO.findEmployeByidEmploye(id);
	}

	/* (non-Javadoc)
	 * @see org.formation.spring.service.IServiceEmploye#findAllEmployeBynomEmploye(java.lang.String)
	 */
	@Override
	public List<Employe> findAllEmployeBynomEmploye(String nom) {
		LOGGER.info("Appel de la methode .findAllEmployeBynomEmploye(nom)");
		return crudEmployeDAO.findAllEmployeBynomEmploye(nom);
	}

	/* (non-Javadoc)
	 * @see org.formation.spring.service.IServiceEmploye#findByLoginAndMdp(java.lang.String, java.lang.String)
	 */
	@Override
	public Employe findByLoginAndMdp(String login, String mdp) {
		LOGGER.info("Appel de la methode .findByLoginAndMdp(login, mdp)");
		return crudEmployeDAO.findByLoginAndMdp(login, mdp);
	}

}
