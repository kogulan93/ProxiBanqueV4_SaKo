package org.formation.spring.service;

import java.util.List;

import org.formation.spring.model.Client;
import org.formation.spring.model.Employe;

/**
 * @author Kogulan - Sarra
 * Interface Service Client regroupant toutes les m�thodes de gestion des clients
 *
 */
public interface IServiceClient {
	public void addClient(Client client);

	public List<Client> listClients();

	public void deleteClient(int idClient);

	public Client editClient(int idClient);

	public void updateClient(Client c);

	public Client selectById(int idClient);

	public List<Client> findAllClientByConseiller(Employe employe);

	public void deleteClient(Client client);

}
