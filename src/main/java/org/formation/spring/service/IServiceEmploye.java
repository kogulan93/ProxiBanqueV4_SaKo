package org.formation.spring.service;

import java.util.List;

import org.formation.spring.model.Employe;

/**
 * @author Kogulan - Sarra
 * Interface Service Client regroupant toutes les m�thodes de gestion des clients
 */
public interface IServiceEmploye {
	public void addEmploye(Employe employe);

	public List<Employe> listEmployes();

	public void deleteEmploye(int idEmploye);

	public Employe editEmploye(int idEmploye);

	public void updateEmploye(Employe c);

	Employe findEmployeByidEmploye(int id);

	List<Employe> findAllEmployeBynomEmploye(String nom);

	Employe findByLoginAndMdp(String login, String mdp);
}
