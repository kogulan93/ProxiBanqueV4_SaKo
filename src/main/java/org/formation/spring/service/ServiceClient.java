package org.formation.spring.service;

import java.util.List;

import javax.annotation.PostConstruct;

import org.formation.spring.dao.CrudClientDAO;
import org.formation.spring.model.Adresse;
import org.formation.spring.model.Client;
import org.formation.spring.model.Conseiller;
import org.formation.spring.model.Employe;
import org.formation.spring.model.TypeClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Kogulan - Sarra
 * Classe Service Client 
 */
@Service("serviceClient")
public class ServiceClient implements IServiceClient {
	private static final Logger LOGGER =  LoggerFactory.getLogger(ServiceClient.class);

	@Autowired
	private CrudClientDAO crudClientDAO;
	
	@Autowired
	private IServiceEmploye servCli;
	
	/**
	 * Creation des Clients dans la base de donnees
	 */
	@PostConstruct
	public void createSomeClient(){
		Conseiller myCons = new Conseiller("Admin", "main", new Adresse(), "admin", "pass");
		servCli.addEmploye(myCons);
		addClient(new Client("Rajendran","Kogulan",new Adresse(52, "rue Gutenberg",93310,"Le Pre Saint Gervais"),"0651218201","kogulan.rajendran@live.fr",TypeClient.PARTICULIER,myCons));
		addClient(new Client("Ronaldo","Cristiano",new Adresse(1,"Avenida de Concha Espina",28036,"Madrid"),"012345678","ronaldo.cristiano@gmail.fr",TypeClient.PARTICULIER,myCons));
		addClient(new Client("Luc","Jean",new Adresse(5,"rue Gargand",92750,"Maner"),"0651218201","Jean.Luc@hotmail.fr",TypeClient.ENTREPRISE,myCons));

		for (Client client : crudClientDAO.findAll()) {
			client.setConseiller(myCons);
			crudClientDAO.save(client);
		}
	}

	/* (non-Javadoc)
	 * @see org.formation.spring.service.IServiceClient#addClient(org.formation.spring.model.Client)
	 */
	@Override
	public void addClient(Client c) {
		crudClientDAO.save(c);
	}

	/* (non-Javadoc)
	 * @see org.formation.spring.service.IServiceClient#listClients()
	 */
	@Override
	public List<Client> listClients() {
		LOGGER.debug("lister clients");
		LOGGER.info("information");
		return crudClientDAO.findAll();
	}

	/* (non-Javadoc)
	 * @see org.formation.spring.service.IServiceClient#deleteClient(int)
	 */
	@Override
	public void deleteClient(int idClient) {
		crudClientDAO.delete(idClient);

	}

	/* (non-Javadoc)
	 * @see org.formation.spring.service.IServiceClient#editClient(int)
	 */
	@Override
	public Client editClient(int idClient) {
		return crudClientDAO.findOne(idClient);
	}

	/* (non-Javadoc)
	 * @see org.formation.spring.service.IServiceClient#updateClient(org.formation.spring.model.Client)
	 */
	@Override
	public void updateClient(Client c) {
		crudClientDAO.save(c);

	}

	/* (non-Javadoc)
	 * @see org.formation.spring.service.IServiceClient#selectById(int)
	 */
	@Override
	public Client selectById(int idClient) {
		return crudClientDAO.findClientByidClient(idClient);
	}

	/* (non-Javadoc)
	 * @see org.formation.spring.service.IServiceClient#findAllClientByConseiller(org.formation.spring.model.Employe)
	 */
	@Override
	public List<Client> findAllClientByConseiller(Employe employe){
		return crudClientDAO.findAllClientByConseiller(employe);
	}

	@Override
	public void deleteClient(Client client) {
		crudClientDAO.delete(client);
	}

	

}
