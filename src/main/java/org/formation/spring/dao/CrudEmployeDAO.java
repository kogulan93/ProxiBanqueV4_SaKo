package org.formation.spring.dao;

import java.util.List;

import org.formation.spring.model.Client;
import org.formation.spring.model.Employe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

/**
 * The implementation of BlogPostDAO interface is not required It is generated
 * by Spring Data JPA Framework This interface is responsible for CRUD + custom
 * queries based on query methods and parameter name and types
 */
// public interface CrudClientDAO extends CrudRepository<Client, Integer>{
public interface CrudEmployeDAO extends JpaRepository<Employe, Integer> {

	// customized methods here
	Employe findEmployeByidEmploye(int id);

	List<Employe> findAllEmployeBynomEmploye(String nom);
	Employe findByLoginAndMdp (String login, String mdp);
}
