package org.formation.spring.dao;

import java.util.List;

import org.formation.spring.model.Compte;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

/**
 * The implementation of BlogPostDAO interface is not required It is generated
 * by Spring Data JPA Framework This interface is responsible for CRUD + custom
 * queries based on query methods and parameter name and types
 */
// public interface CrudClientDAO extends CrudRepository<Client, Integer>{
public interface CrudCompteDAO extends JpaRepository<Compte, Integer> {

	// customized methods here


}
