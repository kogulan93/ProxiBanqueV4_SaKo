package org.formation.spring.test;

import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.formation.spring.config.ApplicationConfig;
import org.formation.spring.dao.CrudClientDAO;
import org.formation.spring.dao.CrudEmployeDAO;
import org.formation.spring.model.Adresse;
import org.formation.spring.model.Client;
import org.formation.spring.model.CompteCourant;
import org.formation.spring.model.Conseiller;
import org.formation.spring.model.TypeClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


/**
 * @author Kogulan - Sarra
 *	Classe de test sur entity Client
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={ApplicationConfig.class})
public class ClientServiceTest {

	
	@Autowired
	private CrudClientDAO crudClientDAO;

	@Autowired
	private CrudEmployeDAO crudConseillerDAO;
	
//	@Test
//	public void testcrudClientDAO() {		
//		assertNotNull(crudClientDAO);		
//	}
//	
//	@Test
//	public void testInsertClient() {
//		Client testCli = new Client("AZERTY","Jean",new Adresse(5,"rue Gargand",92750,"Maner"),"0651218201","Jean.Luc@hotmail.fr",TypeClient.ENTREPRISE);
//		crudClientDAO.save(testCli);
//		List<Client> name = crudClientDAO.findAllClientBynomClient("AZERTY");
//		assertEquals(1, name.size());	
//	}
//	
//	@Test
//	public void testAddComptetoClient() {		
//		Client testCli = crudClientDAO.findOne(1);
//		CompteCourant cc = new CompteCourant(125000,"12/07/2000");
//		cc.setClient(testCli);
//		testCli.setCompteCourant(cc);
//		crudClientDAO.save(testCli);
//		assertEquals(1, crudClientDAO.findClientByidClient(1).getCompteCourant().getClient().getIdClient());
//	}
//	@Test
//	public void testUpdateValuesClient() {		
//		Client testCli = crudClientDAO.findOne(1);
//		String valueBefore = testCli.getNomClient();		
//		testCli.setNomClient("Rajendran");	
//		crudClientDAO.save(testCli);		
//		String valueAfter = crudClientDAO.findOne(1).getNomClient();		
//		assertThat(valueBefore, not(valueAfter));
//	}
//	
//	@Test
//	public void testaddConseillerToClient() {		
//		Client testCli = crudClientDAO.findClientByidClient(1);
//		assertNotNull(testCli);
//		Conseiller cons = (Conseiller) crudConseillerDAO.findEmployeByidEmploye(5);		
//		testCli.setConseiller(cons);
//		crudClientDAO.save(testCli);	
//		assertEquals(crudClientDAO.findClientByidClient(1).getConseiller().getIdEmploye(),5);
//	}
//	
//	@Test
//	public void testdeleteClient() {	
//		Client testCli = crudClientDAO.findClientByidClient(1);
//		crudClientDAO.delete(testCli);
//		assertEquals(crudClientDAO.findClientByidClient(1),null);
//	}
}
