package org.formation.spring.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.formation.spring.config.ApplicationConfig;
import org.formation.spring.dao.CrudClientDAO;
import org.formation.spring.dao.CrudEmployeDAO;
import org.formation.spring.model.Adresse;
import org.formation.spring.model.Client;
import org.formation.spring.model.Conseiller;
import org.formation.spring.model.Employe;
import org.formation.spring.model.Gerant;
import org.formation.spring.model.TypeClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


/**
 * @author Kogulan -Sarra
 * Classe de test sur entity Employe
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={ApplicationConfig.class})
public class EmployeServiceTest {

	
	@Autowired
	private CrudEmployeDAO crudEmployeDAO;

	
//	@Test
//	public void testcrudClientDAO() {		
//		assertNotNull(crudEmployeDAO);		
//	}
//	
//	@Test
//	public void testInsertConseiller() {
//		Employe testEmp = new Conseiller("zuckerberg", "mark", new Adresse(25, "Palo Alto", 85423, "California"), "Mark", "ilovefacebook");
//		crudEmployeDAO.save(testEmp);
//		List<Employe> name = crudEmployeDAO.findAllEmployeBynomEmploye("zuckerberg");
//		assertEquals(1, name.size());	
//	}
//	
//	@Test
//	public void testInsertGerant() {
//		Employe testEmp = new Gerant("Musk", "Elon", new Adresse(25, "avenue Bel Air", 1253, "Los Angeles"), "SpaceX", "ilovenasa");
//		crudEmployeDAO.save(testEmp);
//		List<Employe> name = crudEmployeDAO.findAllEmployeBynomEmploye("Musk");
//		assertEquals(1, name.size());	
//	}
	
//	@Test
//	public void testAddClientToListConseiller() {		
//		Client c1 = new Client("Rajendran","Kogulan",new Adresse(52, "rue Gutenberg",93310,"Le Pre Saint Gervais"),"0651218201","kogulan.rajendran@live.fr",TypeClient.PARTICULIER);
//		Conseiller emp = (Conseiller) crudEmployeDAO.findEmployeByidEmploye(5);		
//		//assertNotNull(emp);
//		emp.addClient(c1);
//		crudEmployeDAO.save(emp);
//
//		
//		Conseiller myCons = new Conseiller("Admin", "main", new Adresse(), "admin", "pass");
//
//	
//		
//	}
	
}
